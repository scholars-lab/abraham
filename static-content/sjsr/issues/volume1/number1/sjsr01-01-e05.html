<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<TITLE>
 The Story of Joseph: A Partial Paradigm of Conflict Resolution
</TITLE>
</HEAD>
 <BODY bgcolor="#000000">
  
  <center>
   <table width="700" align="center" bgcolor="#000000">
    <tr>
     <td width="700"><img src="../../../images/sjsr2.jpg"></td>
     <td>&nbsp; </td>
    </tr>
    <tr>
     <td align="right" valign="top">
      <p><font color="#3300CC"><b><i>Number 1.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
       October 2006&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></b></font> <br>
        &nbsp;</p>
     </td>
     <td></td>
    </tr>
   </table>
  </center>
  
  <table BGCOLOR="#3300CC" BORDER="0" ALIGN="CENTER" CELLPADDING="10"
   CELLSPACING="0" WIDTH="700">
   <tr>
    <td>
     
     <table BGCOLOR="#FFFFF0" BORDER="0" ALIGN="CENTER" CELLPADDING="10"
      CELLSPACING="0">
      <tr>
       <td>



<h2>The
Story of Joseph: A Partial Paradigm of Conflict Resolution</h2>

<i>by
Dennis Beck-Berman</i>

<p><b>Introduction</b></p>

<p>Throughout the book of
Genesis we read of human conflict. 
Indeed, Genesis begins with several stories of fratricidal behavior:
Cain kills Abel, Esau threatens Jacob, and Joseph is nearly killed by his
brothers. Scripture seems to suggest
that since we are all brothers, all murder is fratricide. But we also read
tales of reconciliation. Genesis ends
with Joseph's firstborn Manasseh evincing not the slightest jealousy toward his younger
brother Ephraim, despite their grandfather's blatant favoritism.<a href="#_edn1" name="_ednref1" ><sup>[1]</sup></a> The Joseph saga is a
paradigmatic example. Scriptural
Reasoning (SR) tries to read Scripture as a guide for right living. This essay will explore some lessons that the
Joseph saga may teach us today about resolving conflict
and living together in harmony.</p>

<p>Assuming that the Joseph
saga (Gen. 37-50) is a polished, coherent narrative, we must reasonably
interpret the tale so that all the details present in the story&mdash;as well as
those inexplicably absent&mdash;merge into a meaningful tale. We
should carefully analyze the plot (Why do certain things happen&mdash;or not happen&mdash;which are unexpected?) and the dialogue (Why do characters express themselves
in certain ways?).</p>

<p>There
are several questions that arise from the plot: (1) Why does he conceal his
identity upon meeting his brothers? (2) Why does he accuse them of being spies?
(3) Why doesn't Joseph contact his family after rising to prominence? (4) What are the dynamics of divine
providence? (5) Why do the brothers
never seek Joseph's forgiveness? (6) Why
does Joseph never ask forgiveness from his brothers?  There are lines of exegesis dealing with
these questions, found in the traditional Jewish commentators<a href="#_edn2" name="_ednref2" ><sup>[2]</sup></a> as
the result of a close reading of the text, which reveal Scripture's internal
logic in a way that makes sense of all the details in the story. After exploring these questions I will
attempt to extrapolate a limited scriptural paradigm of conflict resolution.</p>

<p><b>Question 1:
Why does Joseph conceal his identity upon meeting his brothers?</b></p>

<p>Many
Jewish commentators use Maimonides's definition of true repentance
as a hermeneutic to answer the first question.</p>

<p>What
constitutes complete repentance? When one is confronted by the identical
situation wherein he previously sinned, and it lies within his power to commit
the sin again, yet he nevertheless does not succumb because he wishes to repent&mdash;not because he is too fearful or weak [to repeat the sin]. How so? If he had relations
with a woman forbidden to him and he is subsequently alone with her, still in
the throes of his passion for her, and his virility is unabated, and [they are]
in the same place where they previously sinned; if he abstains and does not
sin, this is a true penitent. (<i>Mishneh
Torah</i>, Laws of Repentance, 2:1)</p>



<p>It
seems to me that this approach genuinely emerges from a close reading of the
text.<a href="#_edn3" name="_ednref3" ><sup>[3]</sup></a>
Scripture implies that Joseph put his brothers to the test. He arranged to bring his younger brother into
a similar situation. Benjamin was just
like him, a son of Rachel, the youngest, and his father's favorite (Gen.
44:20). This time the brothers would be faced with a valid excuse to abandon
their little stepbrother. They could not
fight the entire Egyptian empire to save Benjamin from punishment for
theft. But on this occasion their
behavior demonstrated that the brothers were true penitents. Indeed, Judah, whose moral growth is
highlighted in the embedded story of Tamar (Gen. 38:24-26), rejects Joseph's
just offer to punish the guilty one and set the others free, and instead
insists that he, the innocent one, remain as a slave, while Benjamin and the
other brothers are set free, since he is responsible for Benjamin and fears
that their beloved father might die from grief (Gen. 44:30-34). Joseph is simply overwhelmed (Gen. 45:1).</p>

<p><b>Question 2: 
Why does Joseph accuse them of being spies? </b></p>

<p>Jewish
commentators address the second question by explaining that Joseph concealed
his true identity and used various stratagems (accusing them of spying,
secretly returning their money) to rouse them to remorse and to punish them
"measure for measure," albeit only to a limited degree.<a href="#_edn4" name="_ednref4" ><sup>[4]</sup></a> After being cast into the Egyptian prison,
like Joseph, not knowing what would be their fate at the hands of strangers,
the remorseful brothers remember their lack of compassion towards Joseph and
realize that this unexpected turn of events is divine retribution.<a href="#_edn5" name="_ednref5" ><sup>[5]</sup></a> </p>

<blockquote><i>They said to
one another, "Alas, we are being
punished on account of our brother, because we looked on at his anguish, yet
paid no heed as he pleaded with us. That is why this distress has come upon
us."</i> (Gen. 42:21)</blockquote>



<p>The flashback in the narrative at this
juncture represents the awakening of the brothers' conscience. Joseph's pleas well up not from the pit, but
from the depths of their hearts. Its
literary purpose is to reveal the feelings of remorse arising in the
consciousness of the brothers. It occurs
not in the prison, but afterwards, when they were faced once again with the
prospect of returning home to their father with one brother missing.  </p>



<p><b>Question 3: 
Why doesn't Joseph contact his family after rising to prominence?</b></p>

<p>Jewish
commentators convincingly demonstrate that Joseph does not contact his family
after liberation from slavery because he is paralyzed by the prospect of his
brothers' shame.<a href="#_edn6" name="_ednref6"><sup>[6]</sup></a> Indeed, this adds another
dimension to Joseph's reluctance to reveal himself to his brothers right away;
the ensuing guilt and shame on his family would tear them apart.<a href="#_edn7" name="_ednref7"><sup>[7]</sup></a>   But once his brothers appear before him,
Joseph engages in a therapeutic project designed to allow his family to endure
his 'resurrection' without shame. Joseph
wants to validate experimentally the truth of the brothers' words with
proof. Benjamin is to provide physical
evidence of their authenticity.<a href="#_edn8"
name="_ednref8"><sup>[8]</sup></a>   Joseph hopes to provide himself, his
brothers, and his father with empirical evidence that his brothers are truly
changed men. </p>

<p>Much
of his project succeeds, though it may have ended prematurely (see below). When
Joseph showed blatant favoritism toward Benjamin at dinner, the brothers kept
drinking merrily, without a hint of jealousy (Gen. 43:33 ff.).   Joseph's plan demonstrated that the brothers
would not abandon Benjamin, even at the risk of their own imprisonment (Gen.
44:13-16). Moreover, the brothers had
grown spiritually and recognized the workings of divine justice in their lives
(Gen. 42:21, 28; 44:16). </p>

<p>By
the time Joseph revealed himself, they had already come to realize that they
were changed men who truly regretted their younger misdeeds. By articulating a transformed personal
narrative of his own life based on Providence,
Joseph hopes to provide his brothers with a transformed group narrative to tell
their father and defuse the shame and "ground the electric furies of their own
humiliation."<a href="#_edn9" name="_ednref9"><sup>[9]</sup></a></p>

<p><b>
Question 4: 
What are the dynamics of divine providence?</b></p>

<p>Scripture says little about the dynamics of divine providence in
our story. Apparently, the author of the
Joseph saga assumed that his ancient Israelite readers were quite familiar with
such beliefs. The brothers allude to the
workings of divine justice (see n. 5). Providence seems to be a spiritual law of
nature, similar to karma, wherein good is rewarded and evil punished in a
perfect system of divine justice.<a href="#_edn10"
name="_ednref10"><sup>[10]</sup></a> Joseph insists that the lives of people and
nations are under the control of a caring God.<a href="#_edn11" name="_ednref11"><sup>[11]</sup></a>   </p>

<p>Indeed,
the theme of God's guiding
hand underlies the entire story. When
Joseph is lost he meets someone who knows exactly where his brothers are (Gen. 37:15).  The trading caravans happen to be going down
to Egypt (Gen. 37:25, 28). The Lord is with
Joseph in Potiphar's house (Gen. 39:2) and in prison (Gen. 39:21 f.). God's name comes readily to Joseph's lips at
critical moments: when he confronts Potiphar's wife (Gen. 39:9); when he
interprets dreams (Gen. 40:8; 41:16 ff.); and when he tests his brothers (Gen.
42:18). Joseph gives the ultimate
interpretation of events at the dramatic conclusion: "God has sent me ahead of you to ensure your survival on earth, and to
save your lives in an extraordinary deliverance. So, it was not you who sent me here,
but God" (Gen. 45:7-8). <a href="#_edn12" name="_ednref12" ><sup>[12]</sup></a></p>

<p>When Joseph says, "although
you intended me harm, God intended it for good" (Gen. 50:20), he is not simply arguing that God can transform
wicked actions to bring about some gracious end. He is saying more than that.  He is arguing that although you sold me, from
the beginning it was really God who sent me here (Gen. 45:5, 8).  The tension remains as to how the brothers'
wickedness and God's intentions work together. 
Although harmonization of these ideas may be humanly impossible (see
below), the divine intention is what should be the focus. Only that can enable reconciliation. </p>

<p>Behind this scriptural tension is a relational understanding
of truth. Truth is not objective.  Joseph is not denying the truth of his
brothers' previous family narrative; they did indeed behave sinfully toward him
and him toward them. But more than one
truth is possible. He proposes to them
an additional truth, one that he suggests is better suited at this time, under
different circumstances, in light of the perspective of divine Providence&mdash;a truth with the transformative power to heal the past
and change the future. </p>

<p>Joseph descends into Egypt as a powerless slave and
eventually ascends to freedom and power. 
The Joseph saga starts a chain of events that leads to the Israelite
captivity in Egypt and their eventual freedom and empowerment at Mount Sinai. This paradigm of oppression and redemption,
which is the central motif of biblical theology, flows naturally from the
Israelite idea of divine providence. It
a karmic universal law of gravitational reversal: What goes down, must come up. Darkness will always give way to light. God will always redeem the oppressed.</p>

<p><b>
Question 5: 
Why do the brothers never seek Joseph's forgiveness?</b></p>

<p>It
is shocking that Scripture makes no mention of the brothers ever seeking
Joseph's forgiveness. In fact, they maintained an unbroken
silence.<a href="#_edn13" name="_ednref13"><sup>[13]</sup></a> Yet during the seventeen years that passed
since the fateful day of reconciliation, the nagging voice of conscience was
not quieted. When Jacob's death removed
the commanding presence of the patriarch, family cohesion collapsed and the
brothers feared Joseph's revenge for the terrible crime they committed against
him.<a href="#_edn14" name="_ednref14"><sup>[14]</sup></a> Joseph was not entirely successful in achieving a genuine reconciliation with
his brothers.  It
seems that he may have unintentionally ended his project prematurely, because "he could no longer control himself" (Gen.
45:1). </p>

<p>Scripture
reports that when the brothers returned home to Jacob, "they recounted all that Joseph had said
to them" (Gen.
45:27). Apparently they revealed to him
all (or most) of the family's sordid secret. 
Jacob, who was suspicious all along (Gen. 42:4), would surely demand to
know what happened. This is implicit
from their action after Jacob's death: </p>

<blockquote><i>So they sent this message to Joseph, "Before his
death your father left this instruction: So shall you say to Joseph, 'Forgive, I urge you, the offense and guilt
of your brothers who treated you so harshly.' Therefore, please forgive the
offense of the servants of the God of your father." And Joseph was in tears as
they spoke to him. </i>(Gen. 50:16-17)</blockquote>



<p>Even
when they finally beg forgiveness from Joseph, they fear to do so directly;
they put the request into the mouth of their deceased father. When they approached Joseph, they "flung themselves before him, and said, 'We are prepared
to be your slaves'" (Gen.
42:4). They have never forgotten
Joseph's portentous dreams, their hatred at the notion of Joseph ruling over them, and the terrible
results (Gen. 37:5-11). They knew
Josephs divinely inspired power to interpret dreams, and they also knew that
resistance or flight would be futile.  
While Scripture never explains their silence, it seems to imply that
they feared approaching Joseph to beg forgiveness lest he insist on fulfilling
his childhood dream and make them his slaves.</p>

<p>When Joseph first met his brothers, he
concealed his identity and accused them of being spies. Scripture tells us that at that time "he recalled the
dreams that he had dreamed about them"
(Gen. 42:9). Note that he did not bring
to mind all the years of suffering, privation, and loneliness. He recalled his dreams. As a child, Joseph was not an expert dream
interpreter. He did not offer an
interpretation then; his family proffered their interpretation. At that moment, Joseph suddenly realized the
meaning of his childhood dreams. The
sheaves in the field bowing down to him (Gen. 37:7 ff.), like the sheaves in
Pharaoh's dream (Gen. 41:22 ff.), foretold how his family would depend upon
Joseph to provide food for their survival. 
Indeed, this is what he tried to explain to them:</p>

<blockquote><i>Now, do not be distressed or
reproach yourselves because you sold me hither<b>; it was to save life that God sent me ahead of you.</b> It is now two
years that there has been famine in the land, and there are still five years to
come in which there shall be no yield from tilling. <b>God has sent me ahead of you to ensure your survival on earth, and to
save your lives in an extraordinary deliverance.</b></i> (Gen. 45:5-7)</blockquote>



<p>But the brothers were in a state of
shock (Gen. 45:3) and did not understand the full import of Joseph's
words. Years later, they were still
unable to absorb the idea that Joseph had no intention of making them his
slaves. Unfortunately, Joseph's earlier
stratagems of imprisoning them and threatening them with slavery had the effect
of making them afraid and distrustful. 
When their fears rise again to the surface he reiterates his original
point:</p>

<blockquote><i>Besides, although you intended
me harm, God intended it for good,<b> so as
to bring about the present result  the survival of many people. </b>And so,
fear not. I will sustain you and your children. Thus he reassured them,
speaking kindly to them.</i> (Gen. 50:20-21)</blockquote>



<p>Once
again, Joseph stresses his concrete actions in sustaining them and their
families. They may distrust his words,
but by his actions he has demonstrated his true intentions.</p>

<p>Jacob,
too, keeps silent throughout the story. After Jacob learned the truth from his
sons, they all departed for Egypt. At Beer-sheba, "God called to Israel in a vision
by night:... 'I am God, the God of your father. Fear not to go down to Egypt, for I will
make you there into a great nation'" (Gen.
46:2-3). Surely Jacob's fear was the
prophecy to Abraham that his descendants would be enslaved and oppressed in a
foreign land (Gen. 15:13). Jacob
realized that this journey was part of the divine plan, and that the future
enslavement of his children was in some measure punishment for their treatment
of Joseph. No wonder he bore this
knowledge in silence. </p>

<p><b>
Question 6: 
Why does Joseph never ask forgiveness from his brothers?</b></p>

<p>The Joseph saga, it seems, is full of
unexpected silences. Scripture never
explicitly describes Joseph asking forgiveness from his brothers for his own
misbehavior.<a href="#_edn15" name="_ednref15"><sup>[15]</sup></a> But it may hint at this when it states: "So there was no one else about
when Joseph made himself known to his brothers" (Gen.
45:1). The Hebrew <i>hitvada'</i>, 'he made
himself known,' is homonymous with <i>hitvadah</i>,
'he confessed (his sin).' Scripture stresses that this critical reunion occurs
in privacy. It is understandable that
confession and asking forgiveness are private matters, shared only between the
aggrieved parties. Could this be the
reason for Scripture's silence on these matters? Why is the reference veiled,
then? Why not simply state that "Joseph
confessed to his brothers" without revealing the dialogue? Scripture will not reveal her secret here.</p>

<p>It is possible
that there is a hint of Joseph asking forgiveness when he reveals himself to
his brothers. He is so overcome with
emotion, at first all he can say is, <i>"I
am Joseph!" </i>(Gen. 45:3). The reader is left to imagine the thoughts running through
their minds at this moment: Our brother
did not die, nor is he a slave; he is
the viceroy of Egypt and we have bowed before him, just as Joseph predicted in
his childhood dreams; his interrogation and accusations, the money mysteriously
returned to our sacks, his demand for
Benjamin&mdash;all these bizarre events have been part of a secret scheme; but why this scheming when he easily could
have punished us; he seems flustered, but excited rather than angry; he has been testing us, and we have demonstrated
that we are changed men; but what will
we now say to our father?</p>

<p>I imagine a pause as the reality sinks in and the brothers
realize: Joseph is still alive! Then Joseph continues, "<b>My</b><i> father is still alive!"</i><a href="#_edn16" name="_ednref16"><sup>[16]</sup></a> My father, who has
mourned me all these years, is still alive! 
My scheme has succeeded. You can
now face my father without shame and bring him the joyful news.<a href="#_edn17" name="_ednref17" ><sup>[17]</sup></a>  "<i>His
brothers could not respond to him because they were dumbfounded before him"</i>(ibid.). It was all too overwhelming. Sensing their nervousness and confusion, he
tried to reassure them that his goal is reconciliation, not revenge; he wants
the family rupture healed, he yearns to be close to them. "<i>Joseph
said to his brothers, 'Come closer to me,' so they came near. Then he said, 'I am Joseph <b>your brother</b>, whom you sold into Egypt'"</i> (Gen. 45:4). I am still your
brother; I still love you, even though you sold me into slavery. But then
Joseph proceeds to argue that it was all God's doing, not theirs (Gen. 45:5,
8). Scripture seems to suggest that
Joseph was telling his brothers, between the lines:   Just as I am still your brother, and have
shown by my actions that I have forgiven you in my heart for selling me into
Egyptian slavery, so you too, my brothers, should forgive me for the wrongs I
did which provoked your misdeed.</p>

<p><b>A scriptural paradigm
of conflict resolution</b></p>

<p>The title, "A Partial Paradigm of
Conflict Resolution," reflects the structural limitations of the Joseph saga in
addressing conflict resolution. In our
story, the brothers do not actually murder Joseph. Since only a victim can grant forgiveness, we
do not know what would/should have happened in such circumstances. What would/should have happened if the
brothers had not changed? What if they
still envied Benjamin?  What if they
abandoned him? What if, instead of dumbfounded silence, they greeted Joseph's
self-revelation with rage and violence? 
Scripture is silent. Joseph's
therapeutic program is unexpectedly aborted. 
What else would/should he have done to effect complete
reconciliation? Again Scripture is
silent. Hence, we can only extrapolate a
partial paradigm.</p>

<p>There are several elements of conflict
resolution that emerge from Scripture:<a href="#_edn18" name="_ednref18" ><sup>[18]</sup></a>
(1) recognition of one's wrong actions as sins as an act of
intelligence and moral conscience (Gen. 41:21-22, 44:16); (2) remorse, feeling
regret at failure to maintain one's moral standards (ibid.); and (3) desisting
from sin, ceasing the patterns of sinful action to which one was addicted (Gen.
43:34 [jealousy], 44:13-16 [abandonment]). 
There is a hint that
Joseph, too, fulfilled the first element, and he apparently fulfilled the other
two elements of reconciliation toward his brothers.<a href="#_edn19" name="_ednref19" ><sup>[19]</sup></a></p>

<p>Certainly,
in any conflict, both parties must admit wrong for their own misdeeds, and must
struggle to feel remorse for the harm they caused, without rationalizations or
justifications. Each party should
empathize with the victimization of the other, a lesson Joseph taught his
brothers in prison (Gen. 42:21). Yet
each party should show compassion towards the other (ibid.), as they expect it
for themselves. Acceptance of mutual
responsibility does not, however, mean admission of equal guilt. Joseph's crimes were far less severe than
those of his brothers.</p>

<p>Probably
the most powerful element in conflict resolution, however, is the belief in
divine providence, though this exists only in communities of faith which share
this Scriptural tradition. The Joseph
saga shows that it is possible to frame past evil within a paradigm of a divine
plan. The belief in Providence
allows Joseph and his family to transform the way in which they deal with
suffering and those who caused it: "<i>Besides, although you intended me harm,
God intended it for good" (Gen. 50:20). </i>Providence can
provide suffering with cosmic meaning; it is part of a perfect divine plan for
our lives. Despite their evil intentions, those who caused our suffering were
agents of God's will. Providence not
only has the power to provide inspiration and hope to those who suffer, it
actually can redeem suffering. </p>

<p>Once Joseph realized the part
his brothers played in God's plan, he forgave them. It
is said that not to forgive imprisons one in the past and yields control to
another, whereas forgiveness frees the forgiver and allows one to change the
circumstances of one's life. This was
certainly true for Joseph, who was not consumed by anger and desire for
vengeance, unlike Edmund Dantes in <i>The
Count of Monte Cristo</i>.  'Forgive'&mdash;from Old English 'give up (anger)'&mdash;can mean: (1) to excuse
someone for an offense; (2) to renounce anger or resentment against someone for
an offense; (3) to absolve someone from punishment or penalty entailed by an
offense. Joseph apparently forgave his
brothers in all three senses.</p>

<p>From
the viewpoint of divine providence, some (all?) of our mistakes are not simply
crimes awaiting punishment. They are
lessons to be learned. God prefers
repentance and human moral and spiritual growth to punishment. If we change our selves, we can change our
future and transform how we view the past. 
But are we then simply actors on a divine stage, unaware of the roles we
are playing? This is an ancient
paradox: "All is foreseen (by God), but
freedom of choice is given."<a href="#_edn20"
name="_ednref20"><sup>[20]</sup></a> </p>

<p><b>Conclusion</b></p>

<p>Clearly, there are limits to
framing human events in a providential paradigm. Providence
should only be invoked, as it is in the Joseph saga, by the victim, and only by
one for whom it is spiritually meaningful. 
Scripture does not imply that a stranger can invoke Providence to
explain the suffering of others by human evil or natural disaster, and Jews would not interpret the
Holocaust as a refining suffering within a providential plan (though a few
ultra-Orthodox thinkers have done so). 
Furthermore, Providence can
only be invoked in the communities of faith that share this Scriptural
tradition. Hence, it may be useful for Jews,
Christians, and Muslims to move forward in dialogue and reconciliation.</p>

<p>Nearly all conflicts are
ultimately rooted in perceptions of unjust wrongs and conflicting
narratives. The conflict is maintained
by transforming the perpetrator into an inhuman, evil other, then engaging in
retributive actions&mdash;often deemed defensive&mdash;that lead to an unending cycle
of violence. The Israeli-Palestinian
conflict is a typical example. </p>

<p>The scriptural elements of
conflict resolution can provide valuable and effective tools in addressing such
conflicts. Both parties must acknowledge
past wrongs and struggle to feel remorse for the suffering they caused,
without any rationalizations or justifications. Acceptance of mutual
responsibility does not, however, mean admission of equal guilt. Each party should empathize with the
victimization of the other and show compassion towards the other, as they
expect it for themselves. Both parties
must demonstrate their sincerity and changed attitudes by concrete actions, including a
cessation of violence and provocations. </p>

<p>But
perhaps most importantly, both sides must struggle to articulate transformed
national historical
narratives
that reframe their conflict.<a href="#_edn21" name="_ednref21" ><sup>[21]</sup></a>   This would enable each side to appreciate the
justice on the other's side, to see the humanity of the other, to respect the
reasonable goals and deep desires of the other, and allow for the possibility
of reconciliation and peace. Both sides
must seek
reconciliation as brothers, children of Adam and Abraham, rather than obsess
over justice or vengeance for their perceived unjust suffering.</p>

<p>In the case of the
Arab-Israeli conflict, both sides should consider the possibility that
Scripture assigns the Land of Israel to both peoples. Conflicting scriptural promises may
necessitate a creative compromise in understanding the divine plan that allows
them to share the Promised Land. An
example of this approach is Art Waskow's midrash in which the Messiah appears
and declares that instead of destroying the Golden Dome Mosque to make way for
the Third Jewish Temple, God wishes the mosque to become a shared Temple.<a href="#_edn22" name="_ednref22" ><sup>[22]</sup></a>
</p>

<p>Genesis begins with a story of
fratricide and ends with a story of brotherly love and reconciliation. The Bible is an ongoing source of divine
guidance for human life. I have tried to
show in this paper how the Joseph saga provides Scriptural Reasoners with a
partial yet valuable and effective paradigm for conflict resolution.</p>









<hr width="75%">



<p>ENDNOTES</p>

<p><a href="#_ednref1"
name="_edn1" ><sup>[1]</sup></a> <i>JPS
Hebrew-English TANAKH</i> (Philadelphia,
The Jewish Publication Society, 2003) Gen. 48:13-20. Subsequent biblical references are taken from
this text, with some alterations in translation made by me.</p>


<p><a href="#_ednref2" name="_edn2" ><sup>[2]</sup></a>For traditional
Jewish commentators,
I rely primarily on Nehama Leibowitz, <i>Studies
in Genesis</i>, transl. Aryeh Newman, 4<sup>th</sup> rev. ed. (Jerusalem, 1981)
and Avivah Gottlieb Zornberg, <i>The
Beginnings of Desire: Reflections on Genesis </i>(New York, 1995); for early
Jewish sources, on James L. Kugel, <i>The Bible As It Was</i> (Cambridge, 1997).</p>



<p><a href="#_ednref3"
name="_edn3"><sup>[3]</sup></a> See
Leibowitz,
<i>Studies in Genesis</i>, 457-461; Kugel, <i
>The Bible As It Was, </i>265-269.</p>



<p><a href="#_ednref4"
name="_edn4"><sup>[4]</sup></a> 
See

Leibowitz,
<i>Studies in Genesis</i>, 462-68; Kugel,
ibid.</p>



<p><a href="#_ednref5" name="_edn5" ><sup>[5]</sup></a>Gen. 42:28,<i> "And he said to his brothers, "My money has been returned! It is here in my
bag!" Their hearts sank; and, trembling, they turned to one another,
saying, "<b>What is this? God has done (it) to us!</b>" Gen. 44:16,Judah replied, "What can we say to my lord? How can we
plead, how can we prove our innocence? <b>God
has uncovered the crime of your servants.</b> Here we are, then, slaves of my
lord, the rest of us as much as he in whose possession the goblet was found."</i> Simon, seemingly a major
instigator for what happened to Joseph, suffers for several months in prison
(Gen. 42:24, 48:5, 49:5 ff.). </p>



<p><a href="#_ednref6"
name="_edn6" ><sup>[6]</sup></a> 
Zornberg, <i>The Beginnings of Desire, </i>
333-337.</p>


<p><a href="#_ednref7" name="_edn7" ><sup>[7]</sup></a>
Cf.
Gen. 45:24, <i>As he sent his
brothers off on their way, he told them, 
"<b>Do not be quarrelsome</b> on the
way."</i>
</p>



<p><a href="#_ednref8" name="_edn8" ><sup>[8]</sup></a>Gen.
42:15-16,<i> <b>By this you shall be put to the test</b>... that your words <b>may be put to the test whether there is
truth in you</b>..."</i> Gen. 42:20,<i> <b>that
your words may be verified</b> and that you may not die.</i></p>



<p><a href="#_ednref9"
name="_edn9"><sup>[9]</sup></a> 
Zornberg,<i> The Beginnings of Desire, </i>
335.</p>



<p><a href="#_ednref10" name="_edn10" ><sup>[10]</sup></a>
It
is unclear whether the full measure of reward and punishment occurs in this
life, an afterlife, subsequent reincarnations, or to one's descendents.</p>



<p><a href="#_ednref11" name="_edn11" ><sup>[11]</sup></a>Gen.
45:5, <i>Now, do not be distressed or
reproach yourselves because you sold me hither; it was to save life that God
sent me ahead of you.</i> Gen. 45:7-9, <i>God has sent me ahead of you to ensure your
survival on earth, and to save your lives in an extraordinary deliverance.</i> <i>So, it was not you who sent me here, but
God; and He has made me a father to Pharaoh, lord of all his household, and
ruler over the whole land of Egypt. Now,
hurry back to my father and say to him: Thus says your son Joseph, "God has made me lord of all Egypt; come down to me without delay</i>." Gen. 50:19-20, <i>But Joseph said to them, "Have
no fear! Am I a substitute for God? Besides, although you intended me harm, God
intended it for good, so as to bring about the present result&mdash;the survival of
many people."</i></p>



<p><a href="#_ednref12" name="_edn12" ><sup>[12]</sup></a> 
Perhaps
the Psalmist also understood the Joseph story in these terms. See Psalms
105:17-23: "<i>He sent ahead of them a man,
Joseph, sold into slavery. His feet were subjected to fetters; an iron collar
was put on his neck. Until his prediction came true, <b>the decree of the Lord purged him</b>. The king sent to have him freed
... empowered him over all his possessions, <b>to
discipline his princes at will, to teach his elders wisdom."</b> </i>
The
cryptic expression, <i>the decree of the Lord
refined/purged him</i>, seems to imply that Joseph accepted
his imprisonment in Egypt as the will of God, rather than the result of the
evil scheming of Potiphar's wife or the misdeeds of his brothers. The Psalmist also seems to
allude to Joseph's therapeutic project: <i>to
discipline/imprison his </i>[Joseph's]<i>
princes</i> [brothers]<i> at will, to teach
his elders </i>[older brothers]<i> wisdom</i>.</p>



<p><a href="#_ednref13" name="_edn13" ><sup>[13]</sup></a>See
Gen. 45; aside from Gen. 45:15, <i>He kissed
all his brothers and wept upon them; only then<b> were his brothers able to talk
to him</b>.</i></p>



<p><a href="#_ednref14" name="_edn14" ><sup>[14]</sup></a> Gen. 50:15. Cf. Gen. 27:41, <i>Now Esau harbored a grudge against Jacob
because of the blessing which his father had given him, and Esau said to
himself, "Let but the mourning period of my father come, and I will kill my
brother Jacob."</i></p>


<p><a href="#_ednref15" name="_edn15" ><sup>[15]</sup></a>See
Gen. 37 on Joseph's bad reports about his brothers, arrogantly publicizing his
dreams, flaunting his status as father's favorite. </p>


<p><a href="#_ednref16" name="_edn16" ><sup>[16]</sup></a> While usually
translated, <i>"Is my father still alive?"</i>,
it appears here to be an emphatic declaration rather than a question. Joseph
has already asked his brothers,<i> "Is your
father still alive?"</i> (Gen. 43:7, 27) and Judah has just pleaded
that unless Benjamin is released, their father would die (Gen. 44:36-44). What
is the point of asking it again? For the rhetorical use of the interrogative to
express the conviction that a statement is true, see Gesenius-Kautzsch-Cowley,
&para;150 e; cf. Ruth 1:11; Deut. 11:30.</p>



<p><a href="#_ednref17"
name="_edn17" ><sup>[17]</sup></a> Cf. Gen. 45:26, <i>They told him, <b>Joseph is still alive</b> and he is ruler over all the land of Egypt! Jacob was stunned</i>. Gen. 45:28, <i>Then Israel said, "<b>My
son Joseph is still alive</b>!"</i></p>



<p><a href="#_ednref18" name="_edn18" ><sup>[18]</sup></a>These
are similar to the elements of repentance (<i>teshuvah</i>)
&mdash;"return" to one's inner self&mdash;in the rabbinic tradition. See David
R. Blumenthal, <i>Repentance and Forgiveness</i>,
<i>Crosscurrents</i> (Spring, 1998)
[http://www.crosscurrents.org/blumenthal.htm].</p>


<p><a href="#_ednref19" name="_edn19" ><sup>[19]</sup></a> 
If
we assume that behind his tears lay some remorse (Gen. 42:24, 43:30, 45:14-15).
Once he reveals himself, he does not act
arrogantly or insensitively toward his brothers as he did in his youth.</p>


<p><a href="#_ednref20" name="_edn20" ><sup>[20]</sup></a> Mishnah, <i>Avot</i> 3:16.</p>



<p><a href="#_ednref21" name="_edn21" ><sup>[21]</sup></a> Dr. Abdul Abad, a
Palestinian spokesman for Islamic religious councils, articulated a first step
toward such a reframing: "If we see the Holy Land as a wife, we will each say
'she is my wife', and we will continue to fight one another for the right to
claim her. My friends, let us see the Holy Land not as a wife to claim, but
as our mother&mdash;so that we may live in peace as brothers."</p>



<p><a href="#_ednref22" name="_edn22" ><sup>[22]</sup></a> Arthur Ocean Waskow and
Phyllis Ocean Berman, <i>When
Messiah Builds a Temple,</i> http://www.
shalomctr.org/node/309.</p>


<hr width="75%">
<p align="center">&#x00A9; 2006, Society for Scriptural Reasoning</p>
<p align="center"><a href="index.html">Return to Title Page</a></p>
       </td>
      </tr>
     </table>
    </td>
    
  </body>
</HTML>



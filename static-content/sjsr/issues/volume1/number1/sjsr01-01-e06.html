<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<TITLE>
 The Relationships of Scriptural Reasoning: A Conclusion
</TITLE>
</HEAD>
 <BODY bgcolor="#000000">
  
  <center>
   <table width="700" align="center" bgcolor="#000000">
    <tr>
     <td width="700"><img src="../../../images/sjsr2.jpg"></td>
     <td>&nbsp; </td>
    </tr>
    <tr>
     <td align="right" valign="top">
      <p><font color="#3300CC"><b><i>Number 1.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
       October 2006&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></b></font> <br>
        &nbsp;</p>
     </td>
     <td></td>
    </tr>
   </table>
  </center>
  
  <table BGCOLOR="#3300CC" BORDER="0" ALIGN="CENTER" CELLPADDING="10"
   CELLSPACING="0" WIDTH="700">
   <tr>
    <td>
     
     <table BGCOLOR="#FFFFF0" BORDER="0" ALIGN="CENTER" CELLPADDING="10"
      CELLSPACING="0">
      <tr>
       <td>



<h2>The
Relationships of Scriptural Reasoning: A Conclusion</h2>

<i>by
Caitlin Golden</i>

<p>As
the closing of my introductory remarks stress, the scriptural reasoning project
that led to the creation of this journal is by no means complete, even though
the reflections of our four authors are now in print.  During the process of organizing their
thoughts for their written reflections, these four authors raise questions and
discuss the problematic aspects of the texts of the Joseph story with one
another in ways that profoundly influence the versions of the papers that are
compiled in this journal. Thus, as the
introduction suggests, the relational focus of this scriptural reasoning
project reaches beyond the relationships within the scriptural narratives
themselves and between various relevant texts. 
Indeed, an understanding of the relationships among the SR
participants&mdash;whether through the interaction of their varying religious
backgrounds or through the 'dialogue' that occurs now through the compilation
of their four papers&mdash;and of the consequences of SR for relations in the world
today is of the utmost importance in a project that emphasizes the value of
relational forms of study.</p>

<p>When
considering the relationships built between the members of our SR group and the
interactions of the religious backgrounds we bring with us to our discussions,
it is interesting to note that that our respective religious backgrounds did
not come into play overtly at the commencement of our project. As we initially sat down with the scriptural
story we were studying, it was almost as if we were students in a literature
seminar coming together to interpret an assigned text: we slowly read verses,
pondered the questions and problems that arose with each verse, and shared our
thoughts on the possible meanings that arose during the course of our shared
close reading. As we continued to do
this, however, a fascinating change occurred that allowed our imaginations to
ponder creative responses to problematic points in the texts, such as one of
the points at which Joseph begins to weep.<a href="#_edn1" name="_ednref1" ><sup>[1]</sup></a>   In this instance, our individual responses
began to be shaped more evidently by the mental associations we were making
with various aspects of our respective religious traditions. Such associations ranged from a connection
drawn between the weeping Joseph and Christ, to the somewhat baffled question
"what about Judas?" as we discussed treachery and forgiveness in the Joseph
narrative. </p>

<p>It was at this
point in our scriptural reasoning process&mdash;the point at which we were attempting
to respond to the questions that the text itself raises&mdash;that the dialogue among
our various religious traditions became indispensable. Coming to the text with a Roman Catholic
perspective, I found myself turning to my fellow group members for clarification
about the frameworks of religious interpretation from which they were
coming. Even those who were also of
Christian backgrounds were able to open my mind to particularly Protestant
responses to the issues at hand&mdash;responses that certainly contributed to my own
understanding of the way in which the Joseph story can be read in the context
of New Testament values and beliefs. 
Experiencing this process firsthand unquestionably deepened my
understanding of the importance of dialogue among SR participants of different
religious backgrounds; by proposing unique responses to the problematic aspects
of the text and perhaps even identifying problematic aspects that would not
have been noticed by others, each interlocutor brought his or her own religious
tradition into a fruitful relationship with the traditions of the other
participants.</p>

<p>Turning
from the practice that led to the four papers within this journal and toward
the papers themselves, one need not look far in order to see that our dialogue
on the story of Joseph remains incomplete. 
Rather, compiling these papers in the same journal can be seen as an
invitation for both the authors and their readers to continue the dialogue that
this project has only just begun. For
example, simultaneously studying Hamid's "The Human-God Relationship in the
Qur'anic Story of Joseph" and Huffman's "The Agency of God: A Reading of
Joseph" quickly reveals that their
respective approaches to the relationships between human actions and the power
of God can form the basis for a stimulating discussion on divine agency. For Hamid, the contrast between Joseph and
his brothers lies in their willingness or unwillingness to submit themselves
fully to God; Hamid stresses that the human "position of servitude and
surrender should ideally remove the individual ego and allow one to see all of
his/her interactions as a testimony of his/her acknowledgement of God."<a href="#_edn2" name="_ednref2"><sup>[2]</sup></a>   Huffman too emphasizes the position of Joseph
as a servant of God, but she uses language that more clearly stresses that <i>God</i>
is the one in control when Joseph appears to be acting.  Indeed, Huffman argues that "as Joseph reveals
his true nature, the narrator reveals God as the true agent of affairs."<a href="#_edn3" name="_ednref3"><sup>[3]</sup></a> While the remainder of Huffmans paper
stresses that agency belongs to God and that Joseph's brothers do not recognize
this agency, viewing this paper in conjunction with Hamid's emphasis on the
surrender of the individual ego questions the role of human agency in the very
act of submission to God. If, as Huffman
suggests, Joseph is merely the instrument through which God acts, is this
status of submission a result of God's ultimate power or Joseph's choice to
surrender his own free will? While this
is not the place for a detailed examination of free will issues in the Joseph
narrative, it suffices to say that this
point of connection between Hamid and Huffman's papers is an example of one
area in which the relationship between the written responses of our SR group
can in fact lead to even more dialogue.</p>

<p>Similarly,
a study of the relationship between Shepherd's "Sight, Obligation, and
Forgiveness in the Joseph Text" and Beck-Berman's "The Story of Joseph: A
Partial Paradigm of Conflict Resolution" presents another opportunity for
further dialogue on the Joseph story&mdash;in this case, dialogue on the nature of
obligation and forgiveness in human relationships. As stated at the beginning of his article,
Shepherd's primary points of focus are the nature of interpersonal obligations,
the knowledge (or lack thereof) of those obligations, and the possibility of
forgiveness when such obligations are not fulfilled.<a href="#_edn4" name="_ednref4"><sup>[4]</sup></a> Yet while Shepherd's paper concentrates on
the brothers' blindness to their obligations to Joseph (and, to a certain
degree, Benjamin) and the way in which Joseph subsequently forgives them,
Beck-Berman's approach to the matter allows the reader to see a different
aspect of the story&mdash;that of Joseph's need for forgiveness. Indeed, Beck-Berman references Genesis 37 in
order to suggest that Joseph is inappropriately arrogant toward his brothers.<a href="#_edn5" name="_ednref5"><sup>[5]</sup></a> Such an interpretation is not presented by the
other three authors, yet it opens up new realms of possible meanings regarding the
role of mutual forgiveness in human relationships. Thus, just as putting Hamid and Huffman's
articles into dialogue raises even more questions regarding the relationship
between human will and God's agency, so too does a study of the way in which
issues of obligation and forgiveness weave in and out of Shepherd and
Beck-Berman's papers provide the basis for further discussion of the relational
aspects of the Joseph narrative.</p>

<p>After
recalling the introduction's discussion of the role of relationships within the
Joseph story and between relevant texts, as well as recognizing the way in
which the relationships between our authors' religious backgrounds and written
articles lay a firm foundation for fruitful dialogue, we can most appropriately
conclude by reflecting on the possibility that the scriptural reasoning process
has ramifications for relationships in our contemporary world. On a most basic level, the examination of
human-human and human-divine relationships in these four papers provides a
commentary on interaction with others and with the divine that can shape one's
own relationships&mdash;both within the world and with one's God. Beck-Berman's paper most directly addresses
the way in which scriptural reasoning can contribute to a solution of problems
in the world today. While acknowledging
that the Joseph story provides only a partial paradigm for conflict resolution,
Beck-Berman argues that the resolution that occurs between Joseph and his
brothers provides an example of compassion and the acceptance of mutual
responsibility, both of which are of the utmost importance when addressing
issues like the Israeli-Palestinian conflict. 
</p>

<p>Nevertheless,
while Beck-Berman differs from our other three authors in his direct focus on
the possible implications of this SR project in the outside world, the issues
discussed throughout the journal&mdash;whether Hamid's emphasis on submission to God,
Huffman's stress on a recognition of the divine power that lies behind our
acts, or Shepherd's examination of the brothers' ignorance of their obligations
to other humans&mdash;are far from irrelevant when it comes to approaching the problems
that arise in relationships among people and even nations. By exploring the dynamics of relationships
through the lens of the Joseph story, these authors reveal that recognition of
God's power and of one's obligations to one's fellow humans is a prerequisite
for healthy relationships on the human plane and for spiritual development
itself.</p>

<p>Yet
perhaps even more significant than the 'lessons' that the Joseph story, as seen
through the eyes of this journal's authors, teaches its readers is the way in which
the very process of scriptural reasoning personally impacts the relationships
of those involved in it. On one level,
the interaction between members of differing religious traditions has the
potential to create in SR participants a greater appreciation for both the
individuals from whom they are learning and the traditions from which those
individuals come. On an even more
fundamental level, however, scriptural reasoning is a process that teaches
individuals, through its very practice, to approach texts with openness to
polysemy and a willingness to learn through dialogue with those of differing
beliefs. Herein lies the power of
scriptural reasoning in regard to relationships in the contemporary world: by
profoundly shaping the approach to dialogue and relationships taken by
participants in SR, the scriptural reasoning process offers a possible means of
addressing the problems of miscommunication and misunderstanding that too often
plague our world. In this respect, then,
scriptural reasoning is truly a process built on&mdash;and for&mdash;relationships.</p>




<hr width="75%">

<p>ENDNOTES</p>


<p><a href="#_ednref1"
name="_edn1"><sup>[1]</sup></a> <i>JPS
Hebrew-English TANAKH</i> (Philadelphia,
The Jewish Publication Society, 2003) Genesis 45:1-2.</p>



<p><a href="#_ednref2"
name="_edn2"><sup>[2]</sup></a> Asma
Hamid, <a href="sjsr01-01-e02.html">"The Human-God Relationship in the Qur'anic Story of Joseph."</a></p>



<p><a href="#_ednref3"
name="_edn3"><sup>[3]</sup></a> Amanda
Huffman, <a href="sjsr01-01-e03.html">"The Agency of God: A Reading
of Joseph."</a></p>


<p><a href="#_ednref4"
name="_edn4"><sup>[4]</sup></a> Michael
Kelley Shepherd, Jr., <a href="sjsr01-01-e04.html">"Sight, Obligation, and Forgiveness in the Joseph Text."</a></p>


<p><a href="#_ednref5"
name="_edn5"><sup>[5]</sup></a> Dennis
Beck-Berman, <a href="sjsr01-01-e05.html">"The Story of Joseph: A Partial Paradigm of Conflict Resolution."</a></p>


<hr width="75%">

<p align="center">&#x00a9; 2006, Society for Scriptural Reasoning</p>
<p align="center"><a href="index.html">Return to Title Page</a></p>

       </td>
      </tr>
     </table>
    </td>
    
  </body>
</HTML>


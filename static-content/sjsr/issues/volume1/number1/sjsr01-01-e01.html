<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<TITLE>
 The Relationships of Scriptural Reasoning: An Introduction
</TITLE>
</HEAD>
 <BODY bgcolor="#000000">
  
  <center>
   <table width="700" align="center" bgcolor="#000000">
    <tr>
     <td width="700"><img src="../../../images/sjsr2.jpg"></td>
     <td>&nbsp; </td>
    </tr>
    <tr>
     <td align="right" valign="top">
      <p><font color="#3300CC"><b><i>Number 1.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
       October 2006&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></b></font> <br>
        &nbsp;</p>
     </td>
     <td></td>
    </tr>
   </table>
  </center>
  
  <table BGCOLOR="#3300CC" BORDER="0" ALIGN="CENTER" CELLPADDING="10"
   CELLSPACING="0" WIDTH="700">
   <tr>
    <td>
     
     <table BGCOLOR="#FFFFF0" BORDER="0" ALIGN="CENTER" CELLPADDING="10"
      CELLSPACING="0">
      <tr>
       <td><h2>The Relationships of Scriptural Reasoning: An Introduction<BR></h2>
        <i>by
         Caitlin Golden</i>




        <p> Scriptural
         reasoning (SR) is a process built on relationships. As part of such a relational project, the
         readings of the story of Joseph<a href="#_edn1"
          name="_ednref1" ><sup>[1]</sup></a>
         contained within this journal would not be complete without a few remarks on
         four key areas: (1) relationships within the scriptural texts studied, (2)
         intertextual relationships, (3) relationships among the SR participants of this
         group and their respective traditions, and (4) the relationships of our
         contemporary world insofar as they are&mdash;or could be&mdash;affected by the SR
         process. Entering into the scriptural
         study sessions that resulted in the creation of this journal, I initially
         expected that our discussions on the story of Joseph would lead me to a better
         understanding of Muslim, Christian, and Jewish perspectives on
         forgiveness. However, the actual
         experience of participating in the SR process and subsequently delving into the
         four papers of this journal has inspired a shift in my focus away from specific
         traditions' teachings about forgiveness and toward the nature of the
         relationships found in the scriptural reasoning process itself. </p>
        
        <p>It is important to
         note that this shift in focus away from the Muslim, Christian, and Jewish
         doctrines on forgiveness is by no means meant to suggest that the interaction
         among members of these three traditions did not significantly contribute to the
         SR process. Indeed, it was an
         enlightening experience to bring my own Roman Catholic background to the table to
         enter into dialogue with Asma Hamid's largely Sufi approach, Amanda Huffman's
         nondenominational Christian viewpoint, the Virginia Episcopalian perspective of
         Kelley Shepherd, and the thoughts of Dennis Beck-Berman, a Jewish Renewal rabbi
         who has been strongly influenced by the Conservative movement. Nevertheless, my role at this point in our
         scriptural reasoning project is not, as I had initially expected, to study the
         specific views of these four religious perspectives on forgiveness; rather, my
         goal here is to bring out the dynamics of the relationships that occur in SR,
         both within or between the texts and among the people themselves&mdash;SR
         participants and otherwise. In order to
         commence with this project, it is perhaps most useful to center my introductory
         remarks on the first two key areas of SR relationships as they are addressed in
         the four articles that follow: those relationships within the scriptural texts
         and those between various related texts.
         Remarks on the final two types of relationships&mdash;that is, the
         interactions between SR participants/religious traditions and the effects of SR
         on relationships in the world&mdash;are best saved for the conclusion of the journal,
         at which point all four voices of my fellow SR participants will have had a
         chance to speak for themselves.</p>
        
        <p>Asma
         Hamid's reading of the Joseph story in Sura XII of the Qur'an addresses a
         number of themes that are also taken up by the other three authors, and thus it
         is most appropriate to begin with her work on "The Human-God Relationship in
         the Qur'anic Story of Joseph." As the
         title of her article suggests, Hamid devotes a significant portion of her study
         to examining relationships between the human realm and the realm of the divine,
         particularly insofar as Joseph's actions in the world are guided by the
         "unseen."<a href="#_edn2" name="_ednref2"><sup>[2]</sup></a> What is most important to note, however, is
         that Hamid does not concentrate solely on relationships between individual
         humans and God; rather, she approaches the issue of human-divine relationships
         by also reflecting on the relationships that occur among humans
         themselves. As Hamid stresses from the
         very beginning of her article, the Joseph story reveals that relationships
         between human beings and God have definite implications on the plane of human
         relationships; indeed, Hamid writes: "The relationship of Joseph and the
         brothers is a manifestation of the heedlessness of the brothers of their
         relationship to God, as opposed to Joseph's strong awareness and perception of
         the Divine presence." According to this
         view, restoring the "pact" one has with one's fellow human beings is an
         essential part of spiritual development; it is this process of transformation
         that Joseph's brothers must undergo with Joseph as their guide. </p>
        
        <p>Furthermore, in
         the course of examining the connection between human-human relationships and
         human-divine relationships, Hamid explores the respective roles of sight and
         blindness in the Joseph story. She
         particularly concentrates on the way in which Joseph's visions serve as a
         "connection to a higher Divine realm"&mdash;a focus that certainly suggests that the
         motif of sight is intimately tied to the relational aspects of the Joseph
         story. Moreover, in addition to her
         emphasis on the role of relationships within Sura XII's account of Joseph,
         Hamid uses intertextual relationships&mdash;including a reference to a Qur'anic story
         about Moses, a passage from the poetry of Rumi, and Ibn al-'Arabi's remarks on
         man's knowledge of God&mdash;in order to inform her reading of the Qur'anic narration
         of the Joseph story.</p>
        
        <p> In
         "The Agency of God: A Reading of Joseph," Amanda Huffman explores a question
         closely related to Hamid's portrayal of Joseph as one who is guided by the
         unseen: the question of the relationship between God's agency and human
         agency. Huffman places her study in the
         context of the framework presented by Terrence Fretheim, an Old Testament
         Christian scholar, which suggests three possible ways in which God's agency
         could be related to the human world.<a href="#_edn3" name="_ednref3" ><sup>[3]</sup></a> After setting this framework as the
         background for her discussion of agency in the Joseph story, Huffman
         immediately identifies the intertextual approach&mdash;specifically in terms of the
         relationship between the Genesis story of Joseph and its retelling in Acts&mdash;that
         she plans to take in her efforts to deal with questions of agency. Indeed, Huffman later explicitly discusses
         the interesting perspective revealed by the simultaneous study of these two
         versions of the same story: "Through this method of reading the New Testament
         account of Joseph in Acts over top of the Old Testament account of Joseph in
         Genesis, the reader becomes exposed to an entirely new dimension of the
         text. The Acts account serves to clarify
         the Genesis narrative, while the story in Genesis can fill in the missing
         details of Acts." This commentary on the
         process of rereading Genesis through Acts adds an interesting dimension to
         Huffman's focus on relationships&mdash;a focus that she also demonstrates in the way
         in which she distinguishes Joseph from the brothers based on his differing
         relationship with God and his ability to "see" the divine presence. While Joseph's actions and the language of
         the Genesis narrator reveal that God is with Joseph, Huffman argues that "the
         brothers do not feel the presence of God in their privation." Thus, just as the intertextual relationship
         between Genesis and Acts plays a significant role in Huffman's reading of the
         Joseph story, so too do the human-divine relationships in the Joseph story
         itself shape Huffman's conclusions regarding God's agency.</p>
        
        <p> While
         Huffman's emphasis on the agency of God leads her to concentrate on human
         relationships with the divine, Michael Kelley Shepherd, Jr. centers his article
         "Sight, Obligation, and Forgiveness in the Joseph Text" on the obligations that
         humans have to one another, particularly to those to whom they are
         related. Like both Hamid and Huffman,
         Shepherd picks up on the motifs of sight and blindness in the Joseph story;
         however, rather than stressing sight solely as a means of knowing God, Shepherd
         highlights the relationship between sight and knowledge of one's obligations to
         other humans&mdash;a knowledge that Joseph's brothers clearly lack.<a href="#_edn4" name="_ednref4" ><sup>[4]</sup></a> This focus on the obligations inherent in
         human relationships leads Shepherd to describe forgiveness as something that
         can occur only when one can truly "see"; thus, the relative blindness of
         Joseph's brothers prevents them from understanding forgiveness in the way that
         Joseph does. Again, in a manner similar
         to both Hamid and Huffman, Shepherd uses intertextual relationships to enrich
         his study of the Joseph story, particularly drawing from St. Ambrose's
         commentary on the story and the section of Acts to which Huffman also
         refers. Moreover, in studying the role
         of intertextual relationships in the scriptural reasoning process, it is
         especially interesting to note that Shepherd uses the Christian commentary of
         St. Ambrose to broaden the perspective of the Joseph story in Genesis. In other words, Shepherd's references to St.
         Ambrose are not designed to clarify the Genesis story, as are Huffman's
         references to Acts; rather, Shepherd asserts that he turns to St. Ambrose in
         order to bring "a layer of depth" or "an interesting wrinkle" to the story, as
         well as to put the Joseph narrative in a Christian context. Thus, while intertextual relationships may at
         times be seen to serve as tool for the clarification of meaning, so too can
         these relationships among texts contribute to the existence of multiple or more
         complex meanings (i.e. polysemy).</p>
        
        <p> As
         a work with a clear focus on the effects of scriptural reasoning on relationships
         in the contemporary world, Dennis Beck-Berman's "A Partial Paradigm of Conflict
         Resolution" is highly appropriate as the final major article of this
         journal. Yet prior to his emphasis on
         the real-world implications of the SR process, Beck-Berman examines the role of
         relationships within the Joseph story itself&mdash;particularly highlighting, as
         Shepherd does, relationships among humans.
         While Beck-Berman does raise the issues of divine providence that especially
         fascinated Huffman, he identifies the belief in divine providence as "the most
         powerful element in conflict resolution," thus bringing his focus back to
         human-human relationships.<a href="#_edn5"
          name="_ednref5" ><sup>[5]</sup></a> Perhaps the most unique aspect of
         Beck-Berman's study of the element of forgiveness in human relationships is the
         way in which he identifies Joseph as one who, like his brothers, must seek
         forgiveness; by stressing the need of all of the brothers, including Joseph, to
         ask for forgiveness, Beck-Berman presents a situation in which the acceptance
         of mutual responsibility can occur.
         Intertextual relationships are evident in this last paper as well;
         whether referring to traditional Jewish commentators, the Psalms, or the
         Mishnah, Beck-Berman explores both relationships within the Joseph text and
         relationships between that text and other texts with relevant themes.</p>
        
        <p>As
         is evident from this introductory survey of these papers, the human-human and
         human-divine relationships within the texts of the Joseph story&mdash;biblical and
         Qur'anic&mdash;as well as the intertextual relationships that result from the
         comparison of the narrative to other scriptural and non-scriptural sources play
         a significant role in the scriptural reasoning work performed by Hamid,
         Huffman, Shepherd, and Beck-Berman. Yet
         as one reads through their papers and picks up on the themes of sight, divine
         agency, human obligation, and forgiveness contained within these pages, it is
         important to keep in mind that these articles are not works written in
         isolation. Rather, this journal is a
         written continuation of a dialogue in which these authors and I
         participated&mdash;and as such a dialogue, it contains numerous traces of the mutual
         influence of five different perspectives on one another. Moreover, it cannot be stressed enough that these
         papers are the <i>continuation</i> of a dialogue and not merely the record of
         previously held discussions. Therefore,
         my concluding remarks will of necessity focus on the relationships among these
         papers and the SR participants themselves as well as on the possible
         implications of the relationships involved in scriptural reasoning on the world
         in which we live.</p>
        
        
        
        
        
        <hr width="75%">
         
         
         <p>ENDNOTES</p>
         
         <p><a href="#_ednref1"
          name="_edn1" ><sup>[1]</sup></a> <i>JPS
           Hebrew-English TANAKH</i> (Philadelphia,
          The Jewish Publication Society, 2003) Genesis 37-50; <i>The Koran Interpreted</i>,
          trans. A. J. Arberry (New York, Touchstone, 1996) Sura XII.</p>
         
         
         <p><a href="#_ednref2"
          name="_edn2" ><sup>[2]</sup></a> Asma Hamid,
         <a href="sjsr01-01-e02.html"> "The Human-God Relationship in the Qur'anic Story of Joseph."</a></p>
         
         
         <p><a href="#_ednref3"
          name="_edn3" ><sup>[3]</sup></a> Amanda
          Huffman,  <a href="sjsr01-01-e03.html">"The Agency of God: A Reading
          of Joseph."</a></p>
         
         
         
         <p><a href="#_ednref4"
          name="_edn4" ><sup>[4]</sup></a> Michael
          Kelley Shepherd, Jr.,  <a href="sjsr01-01-e04.html">"Sight, Obligation, and Forgiveness in the Joseph Text."</a></p>
         
         
         <p><a href="#_ednref5"
          name="_edn5" ><sup>[5]</sup></a> Dennis
          Beck-Berman,  <a href="sjsr01-01-e05.html">"A Partial Paradigm of Conflict Resolution."</a></p>
         
       <hr width="75%">
<p align="center"> &#x00A9; 2006, Society for Scriptural Reasoning</p>
<p align="center"> <a href="index.html">Return to Title Page</a></p>       
</td>
      </tr>
     </table>
    </td>
    
  </body>
</HTML>

